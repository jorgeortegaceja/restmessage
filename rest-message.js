(function executeRule(current, previous /*null when async*/ ) {

    if (current.operation() == "delete") {
        httpRequest = new sn_ws.RESTMessageV2();
        httpRequest.setHttpMethod('post');
        httpRequest.setRequestHeader('Content-Type', 'application/json');
        httpRequest.setEndpoint('https://demo2-template.es.eastus2.azure.elastic-cloud.com:9243/risks/_delete_by_query');
        httpRequest.setBasicAuth('elastic', 'NGq9UhJBIxKn8JuzNFGl0p4A');


        httpRequest.setRequestBody(JSON.stringify({
            "query": {
                "match": {
                    "sys_id": current.sys_id
                }

            }
        }));


        httpResponse = httpRequest.execute();

    } else {


        var json = {
            'sys_id': current.sys_id.toString().toString(),
            'number': current.number.toString(),
            'active': current.active.toString(),
            'instance': current.instance.toString(),
            'state': current.state.toString(),
            'name': current.name.toString(),
            'description': current.description.toString(),
            'statement': {
                'sys_id': current.statement.sys_id.toString(),
                'name': current.statement.name.toString(),
                'document': current.statement.document.toString(),
                'category': current.statement.category.toString(),
                'description': current.statement.description.toString(),
                'additional_information': current.statement.additional_information.toString()
            },
            'profile': {
                'sys_id': current.profile.sys_id.toString(),
                'refers_to_existing_table': current.profile.refers_to_existing_table.toString(),
                'applies_to': current.profile.applies_to.toString(),
                'active': current.profile.active.toString(),
                'name': current.profile.name.toString(),
                'profile_class': current.profile.profile_class.toString(),
                'owned_by': current.profile.owned_by.toString()
            },
            'category': {
                'sys_id': current.category.sys_id.toString(),
                'set': current.category.set.toString(),
                'choice_category': current.category.choice_category.toString(),
                'name': current.category.name.toString(),
                'order': current.category.order.toString(),
                'label': current.category.label.toString()
            },
            'owning_group': {
                'sys_id': current.owning_group.sys_id.toString(),
                'name': current.owning_group.name.toString(),
                'email': current.owning_group.name.toString(),
            },
            'owner': {
                'sys_id': current.owner.sys_id.toString(),
                'first_name': current.owner.first_name.toString(),
                'last_name': current.owner.last_name.toString(),
                'email': current.owner.toString(),
                'department': current.owner.name.toString(),
                'calendar_integration': current.owner.calendar_integration.toString(),
                'active': current.owner.active.toString(),
                'web_service_access_only': current.owner.web_service_access_only.toString()

            }

        };
	
        var httpRequest = new sn_ws.RESTMessageV2();
        httpRequest.setHttpMethod('GET');
		httpRequest.setRequestHeader('Content-Type', 'application/json');
        httpRequest.setEndpoint('https://demo2-template.es.eastus2.azure.elastic-cloud.com:9243/risks/_search?q=*'+ current.sys_id.toString());
		
        httpRequest.setBasicAuth('elastic', 'NGq9UhJBIxKn8JuzNFGl0p4A');

        var httpResponse = httpRequest.execute();

        var response = JSON.parse(httpResponse.getBody());
		
		
        if (response.hits.total.value > 0) {
            httpRequest = new sn_ws.RESTMessageV2();
            httpRequest.setHttpMethod('post');
            httpRequest.setRequestHeader('Content-Type', 'application/json');
            httpRequest.setEndpoint('https://demo2-template.es.eastus2.azure.elastic-cloud.com:9243/risks/_update/' + response.hits.hits[0]._id);
            httpRequest.setBasicAuth('elastic', 'NGq9UhJBIxKn8JuzNFGl0p4A');
            httpRequest.setRequestBody(JSON.stringify({
                "doc": json
            }));
			
		
            httpResponse = httpRequest.execute();
			
			
        } else {
            httpRequest = new sn_ws.RESTMessageV2();
            httpRequest.setHttpMethod('post');
            httpRequest.setRequestHeader('Content-Type', 'application/json');
            httpRequest.setEndpoint('https://demo2-template.es.eastus2.azure.elastic-cloud.com:9243/risks/_doc/');
            httpRequest.setBasicAuth('elastic', 'NGq9UhJBIxKn8JuzNFGl0p4A');
            gs.log(JSON.stringify(json), 'dev');

            httpRequest.setRequestBody(JSON.stringify(json));


            httpResponse = httpRequest.execute();
        }








        // 	gs.log('response estatus: '+ httpResponse.getStatusCode() ,'dev');
        // 	gs.log(httpResponse.getBody(), 'dev');
        // Return response
        // 	response.setContentType('application/json');
        // 	response.setStatus(httpResponse.getStatusCode());
        // 	response.getStreamWriter().writeString(httpResponse.getBody());
        // 	return response;
    }

    httpRequest = new sn_ws.RESTMessageV2();
    httpRequest.setHttpMethod('get');
    httpRequest.setRequestHeader('Content-Type', 'application/json');
    httpRequest.setEndpoint('https://demo-ipsum.herokuapp.com/api/risks/message');
    httpResponse = httpRequest.execute();

})(current, previous);
